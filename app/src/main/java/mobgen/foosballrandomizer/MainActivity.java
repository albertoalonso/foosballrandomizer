package mobgen.foosballrandomizer;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {


	private boolean sameLengthBothLists=false;


	private String[] attackers = {"Alberto",
			 "Gabor",
			 "Peter M",
			 "Jaime",
			 "Pino",
			 "Céline ",
			 "Dario",
			 "Beth",
			 "Luis Cele",
			 "Jaume",
			 "Liviu"};
	private String[] defenders={"Daniel Gulyas",
			 "Alejandro",
			 "Jose Bustamante",
			 "David de la Viuda",
			 "Daniel Montoya",
			 "Wilco",
			 "Lawrence",
			 "Jorge H",
			 "Pal",
			 "Luca Temperini",
			 "Frank Huffener"};
	private String[] both={"Jorge Araujo","Sascha","Sergio Rodriguez","Matus","Timothey","Dennis"};

	private ArrayList<String> attackersList = new ArrayList<>();
	private ArrayList<String> bothList = new ArrayList<>();
	private ArrayList<String> defendersList = new ArrayList<>();
	private ArrayList<String> finalTeams = new ArrayList<>();
	private Random r = new Random();
	private ListView listView;
	private ArrayAdapter<String> adapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		listView = (ListView) findViewById(R.id.list_view);
		Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finalTeams.clear();
				sameLengthBothLists=false;
				adapter.clear();
				populateList();
			}
		});

		populateList();


	}

	private void populateList() {
		fillLists();
		while(!defendersList.isEmpty() || !attackersList.isEmpty()){

			finalTeams.add(getRandomDefender() + " - " + getRandomAttacker());

		}
		Log.d("alberto","final teams "+finalTeams.toString());
		Log.d("alberto", "final teams size" + finalTeams.size());
		adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_item);
		adapter.addAll(finalTeams);
		listView.setAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	private void fillLists () {


		while(!sameLengthBothLists){

			splitPlayersRandom();

		}

	}


	private void splitPlayersRandom(){
		bothList.clear();
		attackersList.clear();
		defendersList.clear();

		for (String allterrainplayer : both) {
			bothList.add(allterrainplayer);
		}
		for (String attacker : attackers) {
			attackersList.add(attacker);
		}
		for (String defender : defenders) {
			defendersList.add(defender);
		}
		while( !bothList.isEmpty() ){
			String player="foosballPlayer ";

			if (r.nextInt(100) > 50) {
				int position =  getRandomPosition(bothList);
				player = bothList.get(position);
				bothList.remove(position);
				attackersList.add(player);
			} else {
				int position = getRandomPosition(bothList);
				player = bothList.get(position);
				bothList.remove(position);
				defendersList.add(player);
			}
		}

		if(defendersList.size()==attackersList.size()){
			sameLengthBothLists=true;
		}
	}

	private String getRandomAttacker(){
		String attacker="attacker ";

			if(attackersList.size()>0) {

				int position = getRandomPosition(attackersList);
				attacker = attackersList.get(position);
				attackersList.remove(position);
			}
		return attacker;

	}

	private String getRandomDefender(){
		String defender="Defender ";
			if(defendersList.size()>0) {
				int position = getRandomPosition(defendersList);
				defender = defendersList.get(position);
				defendersList.remove(position);
			}
		return defender;
	}

	private int getRandomPosition(ArrayList<String> list){
		int position=0;
		if(list.size()>0){
			position = r.nextInt(list.size());
		}
		return position;
	}
}
